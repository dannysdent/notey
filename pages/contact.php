<?php
require_once('header.php');
include_once("offerSection.php");
require_once('navigation.php');
require_once('sidebar.php');	

$newCompany = new Company();
$company = $newCompany->getCompany();
	
?>

<div id="contact_us">
    <h2>Contact us</h2>
    <form action="?page=email_form" method="post" name="form1" class="contact_form">
        <label for="name"></label>
        <input name="name" type="text" required id="name" placeholder="Name" size="50">
        <br>
        <br>
        <label for="email"></label>
        <input name="email" type="email" required id="email" placeholder="Email" size="50">
        <br>
        <br>
        <label for="comments"></label>
        <textarea name="comments" cols="55" required id="comments"></textarea>
        <br>
        <br>
        <input type="submit" name="submit" id="submit" value="Submit">
    </form>
</div>

<!-- This div will be on the right side - business info -->
<div class="business_info">
	<p><strong><?php echo $company['name']; ?></strong><br>
	<!-- replacing new line tags with <br /> tags -->
	<?php echo nl2br($company['address']); ?><br>
	<?php echo $company['phone']; ?><br>
	<?php echo $company['email']; ?><br>
	<?php echo $company['website']; ?>
	</p>
</div>



<div id="map-canvas"></div>
 
<?php require_once('footer.php'); ?>