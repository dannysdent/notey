<?php
$cat = URL::getParameter('category');

$objProducts = new Products();
$category = $objProducts->getCategory($cat);

require_once('header.php');
require_once('offerSection.php');
require_once('navigation.php');
require_once('sidebar.php'); 

if(!empty($q)){
    $products = $objProducts->getAllProducts($q);
    if(empty($products)){
        $empty = 'Sorry, we were unable to find what you were looking for.';
    }
} else {
    $products = $objProducts->getAll();
    if(empty($products)){
        require_once('no_products.php');
    }
}

if(empty($products)){
    echo '<p>'.$empty.'</p>';
    
} else {
    $paging = new Paging($products);
    $products = $paging->getRecords();
        
?>




    
    <script type="text/javascript" async="" src="css/ec.js"></script>
    <script async="" src="css/bk-coretag.js"></script>
    <script src="css/ytc.js" async=""></script>
    <script src="assetms/857145314392300" async=""></script>
    <script async="" src="css/fbevents.js"></script>
    <script type="text/javascript" async="" src="css/analytics.js"></script>
    <script type="text/javascript" async="" src="css/bat.js"></script>
    <script async="" src="css/gtm.js"></script>
    <script type="text/javascript" async="" src="css/ga.js"></script>
    <!-- storefront -->

<!-- DataLayer -->
<script type="text/javascript" src="css/custom.js" charset="utf-8" async=""></script>




    
<link rel="Stylesheet" href="css/storefronts.css">


<link rel="Stylesheet" href="css/UI_plugins.css">

<link rel="Stylesheet" href="css/CompetitionStorefront.css">

<link rel="stylesheet" href="css/body.css">





<div class="Storefront">
    <!-- Render BreadCrumb -->


    <div class="Background">
        <div id="divStorefrontContent" class="StorefrontContent OverflowFixer">
            <!-- DO NOT REMOVE -->

            <div class="cm_bootstrap">



<div id="cm-placement-topBanner" class="cm-placement-topBanner width100">



  <!-- End Flexslider option - Start Backstretch option -->
  
<div class="clear"></div> 



<h2>Featured products</h2>
<div class="row newsStoryCarousel">

    
<div class="hp-slider-news-story-wrapper hp-slider-wrapper">
<div class="col-xs-12 hp-no-padding">
<div class="hp-no-padding">


<span class="lightslider-control lightslider-prev hp-slider-indented-prev hp-slider-news-story-prevSlide" style="display: none; top: 112.695px;">
</span>

</div>



<div class="lSSlideOuter">




  <div class="lSSlideWrapper usingCss"><ul class="gallery content-slider list-unstyled clearfix lightSlider lsGrab hp-slider-news-story-tiles lSSlide" style="width: 1616.67px; height: 331.172px; padding-bottom: 0%; transform: translate3d(0px, 0px, 0px);">




<li class="lslide" style="width: 313.333px; margin-right: 10px;">

<div class="hp-tile-slider-news-story-4x3">
<div class="hdp-news-story__img-box">
<img class="hdp-news-story__img c-banner-tile__img img-responsive cm-image cm-image--responsive" data-cm-responsive-image="[{&quot;name&quot;:&quot;portrait_ratio4x3&quot;,&quot;ratioWidth&quot;:4,&quot;ratioHeight&quot;:3,&quot;linksForWidth&quot;:{&quot;600&quot;:&quot;/nxtcms/resource/image/128910/portrait_ratio4x3/600/450/fa2d4b820c818cfd332410b1417b57ac/TF/930-605s.jpg&quot;}}]" data-cm-retina-image="true" alt="930-605s" title="" src="images/note0.jpg">

</div> 
<div class="hp-teaser-wrapper">
<h3 class="hp-tile-module-teaser__header">Discovery A6 promotional</h3>
<p class="hp-tile-module-teaser__text">Ksh 3,000</p>
<?php 
            if(!empty($newarrivals)){
              foreach($newarrivals as $cat) {
                echo "<a class='hp-btn hp-tile-slider_portrait-2x3__link-text hp-tile-module-teaser__button' href=\"index.php?page=product&category=43&id=74\"";
                echo Check::getActive(array('category' => $cat['id']));
                echo ">";
                echo "VIEW";
                echo "</a>";
              }
            }
          ?>

 </div>
</div> </li>




<li class="lslide" style="width: 313.333px; margin-right: 10px;">
<div class="hp-tile-slider-news-story-4x3">
<div class="hdp-news-story__img-box">
<img class="hdp-news-story__img c-banner-tile__img img-responsive cm-image cm-image--responsive" data-cm-responsive-image="[{&quot;name&quot;:&quot;portrait_ratio4x3&quot;,&quot;ratioWidth&quot;:4,&quot;ratioHeight&quot;:3,&quot;linksForWidth&quot;:{&quot;600&quot;:&quot;/nxtcms/resource/image/128916/portrait_ratio4x3/600/450/b8a5b90e37c42e2fe88c8c0873793ba/mm/g85-eb-sh400285-lm-1422.jpg&quot;}}]" data-cm-retina-image="true" alt="G85_EB_SH400285_LM_1422" title="" src="images/note1.jpg">
</div> 
<div class="hp-teaser-wrapper">
<h3 class="hp-tile-module-teaser__header">Promotional Nifty Note</h3>
<p class="hp-tile-module-teaser__text">Ksh 2,600</p>
<?php 
            if(!empty($sale)){
              foreach($sale as $cat) {
                echo "<a class='hp-btn hp-tile-slider_portrait-2x3__link-text hp-tile-module-teaser__button' href=\"index.php?page=product&category=45&id=68\"";
                echo Check::getActive(array('category' => $cat['id']));
                echo ">";
                echo "VIEW";
                echo "</a>";
              }
            }
          ?>

 </div>
</div>
 </li>




<li class="lslide" style="width: 313.333px; margin-right: 10px;">
<div class="hp-tile-slider-news-story-4x3">
<div class="hdp-news-story__img-box">
<img class="hp-nhews-story__img c-banner-tile__img img-responsive cm-image cm-image--responsive" data-cm-responsive-image="[{&quot;name&quot;:&quot;portrait_ratio4x3&quot;,&quot;ratioWidth&quot;:4,&quot;ratioHeight&quot;:3,&quot;linksForWidth&quot;:{&quot;600&quot;:&quot;/nxtcms/resource/image/128918/portrait_ratio4x3/600/450/bb6ddeb0f3c43537b64fd77f8e5706de/js/g22-ol-h000-0-530265sm-001.jpg&quot;}}]" data-cm-retina-image="true" alt="G22_OL_H000_0_530265SM_001" title="" src="images/note2.jpg">
</div> </a>
<div class="hp-teaser-wrapper">
<h3 class="hp-tile-module-teaser__header">Swaky Graduation</h3>
<p class="hp-tile-module-teaser__text">Ksh 2,600</p>
<?php 
            if(!empty($today)){
              foreach($today as $cat) {
                echo "<a class='hp-btn hp-tile-slider_portrait-2x3__link-text hp-tile-module-teaser__button' href=\"index.php?page=product&category=43&id=67\"";
                echo Check::getActive(array('category' => $cat['id']));
                echo ">";
                echo "VIEW";
                echo "</a>";
              }
            }
          ?>


</div>
</div>
 </li>






</ul>


</div>

</div>

</div>

</div>





</div>





<div class="row newsStoryCarousel">
<div class="hp-slider-news-story-wrapper hp-slider-wrapper">
<div class="col-xs-12 hp-no-padding">
<div class="hp-no-padding">


<span class="lightslider-control lightslider-prev hp-slider-indented-prev hp-slider-news-story-prevSlide" style="display: none; top: 112.695px;">
</span>

</div>


<div class="lSSlideOuter">
  <div class="lSSlideWrapper usingCss"><ul class="gallery content-slider list-unstyled clearfix lightSlider lsGrab hp-slider-news-story-tiles lSSlide" style="width: 1616.67px; height: 331.172px; padding-bottom: 0%; transform: translate3d(0px, 0px, 0px);">




<li class="lslide" style="width: 313.333px; margin-right: 10px;">

<div class="hp-tile-slider-news-story-4x3">
<div class="hdp-news-story__img-box">
<img class="hdp-news-story__img c-banner-tile__img img-responsive cm-image cm-image--responsive" data-cm-responsive-image="[{&quot;name&quot;:&quot;portrait_ratio4x3&quot;,&quot;ratioWidth&quot;:4,&quot;ratioHeight&quot;:3,&quot;linksForWidth&quot;:{&quot;600&quot;:&quot;/nxtcms/resource/image/128910/portrait_ratio4x3/600/450/fa2d4b820c818cfd332410b1417b57ac/TF/930-605s.jpg&quot;}}]" data-cm-retina-image="true" alt="930-605s" title="" src="images/note3.jpg">
</div> 
<div class="hp-teaser-wrapper">
<h3 class="hp-tile-module-teaser__header">Swanky Doctor Promotional</h3>
<p class="hp-tile-module-teaser__text">Ksh 3,000</p>
<?php 
            if(!empty($newarrivals)){
              foreach($newarrivals as $cat) {
                echo "<a class='hp-btn hp-tile-slider_portrait-2x3__link-text hp-tile-module-teaser__button' href=\"index.php?page=product&category=45&id=58\"";
                echo Check::getActive(array('category' => $cat['id']));
                echo ">";
                echo "VIEW";
                echo "</a>";
              }
            }
          ?>

 </div>
</div> </li>




<li class="lslide" style="width: 313.333px; margin-right: 10px;">
<div class="hp-tile-slider-news-story-4x3">
<div class="hdp-news-story__img-box">
<img class="hdp-news-story__img c-banner-tile__img img-responsive cm-image cm-image--responsive" data-cm-responsive-image="[{&quot;name&quot;:&quot;portrait_ratio4x3&quot;,&quot;ratioWidth&quot;:4,&quot;ratioHeight&quot;:3,&quot;linksForWidth&quot;:{&quot;600&quot;:&quot;/nxtcms/resource/image/128916/portrait_ratio4x3/600/450/b8a5b90e37c42e2fe88c8c0873793ba/mm/g85-eb-sh400285-lm-1422.jpg&quot;}}]" data-cm-retina-image="true" alt="G85_EB_SH400285_LM_1422" title="" src="images/note4.jpg">
</div> 
<div class="hp-teaser-wrapper">
<h3 class="hp-tile-module-teaser__header">Flux Corporate notebook</h3>
<p class="hp-tile-module-teaser__text">Ksh 2,700</p>
<?php 
            if(!empty($sale)){
              foreach($sale as $cat) {
                echo "<a class='hp-btn hp-tile-slider_portrait-2x3__link-text hp-tile-module-teaser__button' href=\"index.php?page=product&category=46&id=57\"";
                echo Check::getActive(array('category' => $cat['id']));
                echo ">";
                echo "VIEW";
                echo "</a>";
              }
            }
          ?>

 </div>
</div>
 </li>




<li class="lslide" style="width: 313.333px; margin-right: 10px;">
<div class="hp-tile-slider-news-story-4x3">
<div class="hdp-news-story__img-box">
<img class="hp-nhews-story__img c-banner-tile__img img-responsive cm-image cm-image--responsive" data-cm-responsive-image="[{&quot;name&quot;:&quot;portrait_ratio4x3&quot;,&quot;ratioWidth&quot;:4,&quot;ratioHeight&quot;:3,&quot;linksForWidth&quot;:{&quot;600&quot;:&quot;/nxtcms/resource/image/128918/portrait_ratio4x3/600/450/bb6ddeb0f3c43537b64fd77f8e5706de/js/g22-ol-h000-0-530265sm-001.jpg&quot;}}]" data-cm-retina-image="true" alt="G22_OL_H000_0_530265SM_001" title="" src="images/note5.jpg">
</div> </a>
<div class="hp-teaser-wrapper">
<h3 class="hp-tile-module-teaser__header">Milestone Corporate</h3>
<p class="hp-tile-module-teaser__text">Ksh 3,000</p>
<?php 
            if(!empty($today)){
              foreach($today as $cat) {
                echo "<a class='hp-btn hp-tile-slider_portrait-2x3__link-text hp-tile-module-teaser__button' href=\"index.php?page=product&category=46&id=77\"";
                echo Check::getActive(array('category' => $cat['id']));
                echo ">";
                echo "VIEW";
                echo "</a>";
              }
            }
          ?>


</div>
</div>
 </li>






</ul>


</div>

</div>

</div>

</div>





</div>



</a>
    </div>
<?php require_once('footer.php'); 
}
?>






