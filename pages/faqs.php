<?php
require_once('header.php');
require_once('offerSection.php');
require_once('navigation.php');
?>

<div id="content" class="row">	
      
      	
      
      <div class="section-title">
  <h1 class="desktop-5 mobile-3">FAQ'S</h1>
</div>


<p><b>WILL I BE CHARGED CUSTOMS AND IMPORT DUTIES?</b></p>

<p>Customs or import duties are charged once the parcel reaches the destination and this charge must be paid by the customer. Unfortunately, Notey has no control over these charges, customs policies and import duties vary widely from country to country so we cannot gee an exact figure of the fee you will incur

You may wish to contact your local customs office for current charges before you order form our website, so you will not be surpassed by charges when your parcel arrives.</p>

<p><b>REFUSAL OF PARCEL</b></p>

<p>If you refuse a parcel, please note you (the customer) will be liable for the return costs and custom charges if applicable. These charges will be deducted from the cost of your order.</p>

 

<p><b>IF MY PARCEL IS LOST WILL I GET REIMBURSED?</b></p>

<p>If you have not received your order within the delivery time frame, we will need to raise a full investigation with our courier which can take up to 2 weeks for them to confirm the details of your order. If you order is confirmed as lost, we will re-despatch your order.</p>

<p><b>CAN MY PACKAGE BE DELIVERED TO A WORK ADDRESS?</b></p>

<p>Packages can be delivered to your place of work however Notey does not accept liability if the parcel has been signed for on your behalf by a member at that address.</p>

<p><b>AN ITEM IS MISSING FROM MY ORDER?</b></p>

<p>If you receive the wrong item or something is missing form your order then please contact our Customer Care Team: info@notey.co.ke</p>












</div>
<?php require_once('footer.php'); ?>