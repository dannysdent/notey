<?php

$id = URL::getParameter('id');

if(!empty($id)){
	$form = new Form();
	$objProducts = new Products();
	$product = $objProducts->getProduct($id);
	
	if(!empty($product)){
		$category = $objProducts->getCategory($product['category']);
		require_once('header.php');
		require_once("offerSection.php");
		require_once('navigation.php');
		require_once('sidebar.php');
?>
	<!-- Display the path of the page on the website -->
	<table class="navArrow navProduct">
		<tr>
			<td>
				<a href="?page=index">Home</a>
			</td>
			<td>
				<img src="images/nav_arrow.gif" alt="" />
			</td>
			<td>
				<a href="?page=categories&amp;category=<?php echo $category['id']; ?>"><?php echo $category['name']; ?></a>
			</td>
			<td>
				<img src="images/nav_arrow.gif" alt="" />
			</td>
			<td>
				<p><?php echo $product['name']; ?></p>
			</td>
		</tr>
	</table>

<?php

include_once('sidebar.php'); 
?>





<!-- Product information -->
<link rel="stylesheet" href="zoom/css/etalage.css" type="text/css" media="all" />


<script src="zoom/js/jquery.etalage.min.js"></script>
<script>
			jQuery(document).ready(function($){

				$('#etalage').etalage({
					thumb_image_width: 300,
					thumb_image_height: 400,
					source_image_width: 900,
					source_image_height: 1200,
					show_hint: true,
					click_callback: function(image_anchor, instance_id){
						alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
					}
				});

			});
		</script>








      <table id="product">
	<tr>
<td>
<div class=" single_top">
	      <div class="single_grid">
				<div class="grid images_3_of_2">
						<ul id="etalage">
							<li>

								<a href="optionallink.html">
									
								
									<?php 
											$image = !empty($product['image']) ? 'images/products/'.$product['image'] : 'images/ImageUnavailable.png';

											if(!empty($image)){
												echo "<img class=\"etalage_thumb_image\" src=\"{$image}\" class=\"img-responsive\" alt=\"";
												echo Check::encodeHTML($product['name'], 1);
												echo "\" />";
											}					
									?>

									<?php 
											$image = !empty($product['image']) ? 'images/products/'.$product['image'] : 'images/ImageUnavailable.png';

											if(!empty($image)){
												echo "<img class=\"etalage_source_image\" src=\"{$image}\" class=\"img-responsive\" alt=\"";
												echo Check::encodeHTML($product['name'], 1);
												echo "\" />";
											}					
									?>
									
								</a>
							</li>
							
						</ul>
						 	
				  </div> 
			<div class="desc1 span_3_of_2">
				  
				

	<td>
		<p class="title"><?php echo $product['name']; ?></p>
		<table class="choose">
			<tr>
				<td>
					<p>Size</p>
					<?php 
					if(!empty($product['size_number'])){
						echo $form->getSizeNumber();
					} else {
						echo $form->getSizeLetter();
					}
					?>	
					
				</td>
			</tr>
			<tr>
				<td>
					<p class="price">
						<?php echo "Ksh :".$product['price']; ?>
					</p>
					<br>
					<p>
					<?php echo Basket::activeButton($product['id']); ?>
					</p>
				</td>
				<td>
					
				</td>

			</tr>



		</table>
	</td>
</tr>
<tr>
	<td colspan="2">
		<div>
		<p class="info">Description:</p>
			<?php echo Check::encodeHTML($product['description']); ?>
		</div>
	</td>
</tr>
<tr>
	<td>
		<p>
			<br>
			<?php
				//Go back one page on click.
				echo "<a class=\"go_back\" href=\"javascript:history.go(-1)\">&laquo; Back</a>"; 
			?>
		</p>
	</td>
				
			   
				
				</div>
          	    
          	   </div>
          	
	
	

          	    
          	   </div>





</td>


</tr>
</table>
















<br>
	
<?php			
		require_once('footer.php');
	} else {
		require_once('error.php');
	}
	

} else {
	require_once('error.php');
}


