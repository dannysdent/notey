<?php
$cat = URL::getParameter('category');

$objProducts = new Products();
$category = $objProducts->getCategory($cat);

require_once('header.php');
require_once('offerSection.php');
require_once('navigation.php');
require_once('sidebar.php'); 

if(!empty($q)){
	$products = $objProducts->getAllProducts($q);
	if(empty($products)){
		$empty = 'Sorry, we were unable to find what you were looking for.';
	}
} else {
	$products = $objProducts->getAll();
	if(empty($products)){
		require_once('no_products.php');
	}
}

if(empty($products)){
	echo '<p>'.$empty.'</p>';
	
} else {
	$paging = new Paging($products);
	$products = $paging->getRecords();
		
?>

<p>Work in progress</p>

<?php require_once('footer.php'); 
}
?>






