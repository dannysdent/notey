<?php

require_once('header.php');
require_once('navigation.php');
require_once('sidebar.php');

?>


<form action="pesapal/pesapal-iframe.php" method="post">
	<table>
		<h2>PesaPal Checkout</h2>
		<tr>
			<th>
				<label>First Name: <span>*</span></label>
			</th>
			<td><input type="text" name="first_name" value="" required/></td>
		</tr>

		<tr>
			<th><label>Last Name: <span>*</span></label></th>

			<td><input type="text" name="last_name" value="" required/></td>
		</tr>
		<tr>
			<th><label>Email Address: <span>*</span></label></th>
			<td><input type="text" name="email" value="" required/></td>
		</tr>

		<tr>
			<th><label>Total Amount: <span>*</span></label></th>
			<td><input type="text" name="amount" value="<?php echo $basket->total_value; ?>" readonly/>
			</td>
		</tr>
		<tr>
			<th><label>Type: <span></span></label></th>
			<td><input type="text" name="type" value="MERCHANT" readonly="readonly" />
			</td>
		</tr>
		<tr>
			<th><label>Description: <span>*</span></label></th>
			<td><input type="text" name="description" value="<?php echo $product['name']; ?>" required/></td>
		</tr>
		<tr>
			<th><label>ID Number: <span>*</span></label></th>
			<td><input type="text" name="reference" value="" required/>
			
			</td>
		</tr>

		<tr>
			<th>&nbsp;</th>
			<td>
				<label for="reg_button" class="reg_button">
					<input type="submit" id="reg_button"
						   class="log_reg_btn" value="Make Payment" />
				</label>
			</td>
		</tr>


	</table>
</form>


<?php
require_once('footer.php');
?>