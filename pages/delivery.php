<?php
require_once('header.php');
require_once('offerSection.php');
require_once('navigation.php');
?>

<div id="content" class="row">	
      
      	
      
      <div class="section-title">
  <h1 class="desktop-12 mobile-3">Delivery</h1>
</div>

<table width="434" height="254">
<tbody>
<tr>
<td>
<p><span style="color: #666666;"> AREA </span></p>
</td>
<td>
<p><span style="color: #666666;"> DELIVERY TIME </span></p>
</td>
<td>
<p><span style="color: #666666;">  COSTS </span></p>
</td>
</tr>
<tr>
<td>
<p><span style="color: #666666;"> NAIROBI CBD </span></p>
</td>
<td>
<p><span color="#666666" style="color: #666666;">3 - 5 hours after order</span></p>
</td>
<td>
<p><span style="color: #666666;"> N0 COST</p>
</td>
</tr>
<tr>
<td>
<p><span style="color: #666666;">NAIROBI OUTSKIRTS</span></p>
</td>
<td>
<p><span style="color: #666666;">3 - 7 hours after order</span></p>
</td>
<td>
<p><span style="color: #666666;">Ksh 250</span></p>
</td>
</tr>
<tr>
<td>
<p><span color="#666666" style="color: #666666;">OTHER TOWNS IN KENYA</span></p>
</td>
<td>
<p><span style="color: #666666;">2 - 5 working days</span></p>
</td>
<td>
<p><span style="color: #666666;">Dependent on Courier</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="color: #666666;"> EAST AFRICA *</span></p>
</td>
<td>
<p><span style="color: #666666;">5 - 10 working days</span></p>
</td>
<td>
<p><span style="color: #666666;">Dependent on Courier</span></p>
</td>
</tr>
<tr>
<td>
<p><span style="color: #666666;"> OUTSIDE EAST AFRICA *</span></p>
</td>
<td>
<p><span color="#666666" style="color: #666666;">14 - 21 working days</span></p>
</td>
<td>
<p><span style="color: #666666;">Dependent On Courier</span></p>
</td>
</tr>
</tbody>
</table>





<p>A working day is Monday to Friday. (Excludes weekends and bank holidays)</p>

<p>* Please be aware that your order will have to go through customs and they may hold on to the order for a number of days. Notey is not liable for any customs charges </p>

<p>If you go to check out and your Country is not there please contact us at info@notey.co.ke and we can look in to adding your Country for delivery.</p>


</div>
<?php require_once('footer.php'); ?>