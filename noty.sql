-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 30, 2018 at 09:28 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `noty`
--
CREATE DATABASE IF NOT EXISTS `noty` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `noty`;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'Islam', 'Dudaev', 'islam89uk@gmail.com', 'islam895'),
(4, 'Daniel', 'Sirali', 'granny@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `category` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `category`) VALUES
(25, 'Get Started', 'dresses'),
(28, 'Sneakers', 'tops'),
(31, 'Laceups', 'tops'),
(32, 'Boots', 'tops'),
(33, 'Moccasins', 'tops'),
(81, 'Accesories', 'accesories'),
(43, 'Best Sellers', 'shoes'),
(45, 'Staff Picks', 'shoes'),
(46, 'Just Published', 'shoes'),
(57, 'Versace', 'bags'),
(58, 'Lacoste', 'bags'),
(59, 'Carrera', 'bags'),
(63, 'Sparco', 'bags'),
(64, 'Pierre Cardi', 'bags'),
(65, 'Paris Hilton', 'bags'),
(66, 'Laura Biagiotti', 'bags'),
(67, 'Watch', 'mens'),
(68, 'Tie', 'mens'),
(69, 'Photo Books', 'outdoor'),
(70, 'Trade Books', 'outdoor'),
(71, 'Magazines', 'outdoor'),
(77, 'Ebooks', 'outdoor'),
(78, 'New Arrivals', 'newarrivals'),
(79, 'Sale', 'sale'),
(80, 'Today''s Deals', 'today');

-- --------------------------------------------------------

--
-- Table structure for table `colours`
--

CREATE TABLE IF NOT EXISTS `colours` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `colour` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `colours`
--

INSERT INTO `colours` (`id`, `colour`) VALUES
(1, 'White'),
(2, 'Black'),
(3, 'Navy'),
(4, 'Blue'),
(5, 'Grey'),
(6, 'Green'),
(7, 'Red'),
(8, 'Brown'),
(9, 'Pink'),
(10, 'Yellow'),
(11, 'Indigo'),
(12, 'Lead'),
(13, 'Chocolate');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `name`, `address`, `phone`, `email`, `website`) VALUES
(1, 'Notey', '123 Winston Park<br>Nairobi', '0705672442', 'info@notey.co.ke', 'www.notey.co.ke');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE IF NOT EXISTS `discounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) NOT NULL,
  `value` decimal(8,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `discounts`
--

INSERT INTO `discounts` (`id`, `code`, `value`) VALUES
(1, 'DIS10', '0.10'),
(2, 'DIS15', '0.15'),
(3, 'DIS20', '0.20'),
(4, 'DIS25', '0.25'),
(5, 'DIS50', '0.50');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `total` decimal(8,2) NOT NULL,
  `date` datetime NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  `paypal_status` tinyint(1) NOT NULL DEFAULT '0',
  `txn_id` varchar(100) DEFAULT NULL,
  `payment_status` varchar(100) DEFAULT NULL,
  `ipn` text,
  `response` varchar(100) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `status` (`status`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user`, `total`, `date`, `status`, `paypal_status`, `txn_id`, `payment_status`, `ipn`, `response`, `notes`) VALUES
(1, 1, '80.00', '2015-11-01 16:32:00', 1, 0, NULL, NULL, NULL, NULL, NULL),
(2, 1, '25.00', '2015-11-01 18:49:01', 1, 0, NULL, NULL, NULL, NULL, NULL),
(3, 1, '18.00', '2015-11-01 19:00:36', 1, 0, NULL, NULL, NULL, NULL, NULL),
(4, 1, '18.00', '2015-11-01 20:28:55', 1, 0, NULL, NULL, NULL, NULL, NULL),
(5, 2, '18.00', '2015-11-01 20:34:55', 1, 0, NULL, NULL, NULL, NULL, NULL),
(6, 2, '18.00', '2015-11-01 22:01:27', 1, 0, NULL, NULL, NULL, NULL, NULL),
(7, 2, '25.00', '2015-11-01 22:17:08', 1, 0, NULL, NULL, NULL, NULL, NULL),
(8, 2, '18.00', '2015-11-01 22:24:29', 1, 1, '40P32712R8153722C', 'Completed', 'mc_gross : 18.00\nprotection_eligibility : Eligible\naddress_status : confirmed\nitem_number1 : 5\npayer_id : WBMAT5GBGJ74Y\ntax : 0.00\naddress_street : sdfdsk\r\ndsf;lk\npayment_date : 14:24:43 Nov 01, 2015 PST\npayment_status : Completed\ncharset : windows-1252\naddress_zip : sdf;kl\nmc_shipping : 0.00\nmc_handling : 0.00\nfirst_name : Islam\nmc_fee : 0.81\naddress_country_code : GB\naddress_name : islam dudaeb\nnotify_version : 3.8\ncustom : 8\npayer_status : verified\nbusiness : acjx557-merchant@gmail.com\naddress_country : United Kingdom\nnum_cart_items : 1\nmc_handling1 : 0.00\naddress_city : dsfds\nverify_sign : AAqF1Yl.5zCXDVuyEillgHdJMwtSALLJt8yioHBr-B8VKMDqdXCcBJ2Y\npayer_email : acjx557-customer1@gmail.com\nmc_shipping1 : 0.00\ntax1 : 0.00\ntxn_id : 40P32712R8153722C\npayment_type : instant\nlast_name : Customer\naddress_state : sdpioepwo\nitem_name1 : Crew T-shirt\nreceiver_email : acjx557-merchant@gmail.com\npayment_fee : \nquantity1 : 1\nreceiver_id : 57MAV7KHDUDY6\ntxn_type : cart\nmc_gross_1 : 18.00\nmc_currency : GBP\nresidence_country : GB\ntest_ipn : 1\ntransaction_subject : 8\npayment_gross : \nipn_track_id : faba79ba908bc', 'VERIFIED', NULL),
(9, 3, '76.00', '2015-11-01 22:32:23', 1, 0, NULL, NULL, NULL, NULL, NULL),
(10, 2, '25.00', '2015-11-02 15:51:50', 1, 1, '7YG46812BY370390G', 'Completed', 'mc_gross : 25.00\nprotection_eligibility : Eligible\naddress_status : confirmed\nitem_number1 : 4\npayer_id : WBMAT5GBGJ74Y\ntax : 0.00\naddress_street : sdfdsk\r\ndsf;lk\npayment_date : 07:52:04 Nov 02, 2015 PST\npayment_status : Completed\ncharset : windows-1252\naddress_zip : sdf;kl\nmc_shipping : 0.00\nmc_handling : 0.00\nfirst_name : Islam\nmc_fee : 1.05\naddress_country_code : GB\naddress_name : islam dudaeb\nnotify_version : 3.8\ncustom : 10\npayer_status : verified\nbusiness : acjx557-merchant@gmail.com\naddress_country : United Kingdom\nnum_cart_items : 1\nmc_handling1 : 0.00\naddress_city : dsfds\nverify_sign : ACACPbgKSl.42IeOt.XH2hhD6fNaAUFVonUJluDIm4UMEstJfwR6NbWv\npayer_email : acjx557-customer1@gmail.com\nmc_shipping1 : 0.00\ntax1 : 0.00\ntxn_id : 7YG46812BY370390G\npayment_type : instant\nlast_name : Customer\naddress_state : sdpioepwo\nitem_name1 : Ella Skinny Jeans\nreceiver_email : acjx557-merchant@gmail.com\npayment_fee : \nquantity1 : 1\nreceiver_id : 57MAV7KHDUDY6\ntxn_type : cart\nmc_gross_1 : 25.00\nmc_currency : GBP\nresidence_country : GB\ntest_ipn : 1\ntransaction_subject : 10\npayment_gross : \nipn_track_id : d7b5f508ae39b', 'VERIFIED', NULL),
(11, 2, '25.00', '2015-11-02 17:37:23', 1, 1, '1D364613HD022600L', 'Completed', 'mc_gross : 25.00\nprotection_eligibility : Eligible\naddress_status : confirmed\nitem_number1 : 4\npayer_id : WBMAT5GBGJ74Y\ntax : 0.00\naddress_street : sdfdsk\r\ndsf;lk\npayment_date : 09:37:38 Nov 02, 2015 PST\npayment_status : Completed\ncharset : windows-1252\naddress_zip : sdf;kl\nmc_shipping : 0.00\nmc_handling : 0.00\nfirst_name : Islam\nmc_fee : 1.05\naddress_country_code : GB\naddress_name : islam dudaeb\nnotify_version : 3.8\ncustom : 11\npayer_status : verified\nbusiness : acjx557-merchant@gmail.com\naddress_country : United Kingdom\nnum_cart_items : 1\nmc_handling1 : 0.00\naddress_city : dsfds\nverify_sign : ApBHX6qbpxJW-Ll3oP22LSbo0WeuAJDhHNv1VyyAZ.NS.fGd5j2igiea\npayer_email : acjx557-customer1@gmail.com\nmc_shipping1 : 0.00\ntax1 : 0.00\ntxn_id : 1D364613HD022600L\npayment_type : instant\nlast_name : Customer\naddress_state : sdpioepwo\nitem_name1 : Ella Skinny Jeans\nreceiver_email : acjx557-merchant@gmail.com\npayment_fee : \nquantity1 : 1\nreceiver_id : 57MAV7KHDUDY6\ntxn_type : cart\nmc_gross_1 : 25.00\nmc_currency : GBP\nresidence_country : GB\ntest_ipn : 1\ntransaction_subject : 11\npayment_gross : \nipn_track_id : bdd4ee86f12fe', 'VERIFIED', NULL),
(12, 2, '18.00', '2015-11-02 17:39:20', 1, 1, '21C8183576407554Y', 'Completed', 'mc_gross : 18.00\nprotection_eligibility : Eligible\naddress_status : confirmed\nitem_number1 : 5\npayer_id : WBMAT5GBGJ74Y\ntax : 0.00\naddress_street : sdfdsk\r\ndsf;lk\npayment_date : 09:39:36 Nov 02, 2015 PST\npayment_status : Completed\ncharset : windows-1252\naddress_zip : sdf;kl\nmc_shipping : 0.00\nmc_handling : 0.00\nfirst_name : Islam\nmc_fee : 0.81\naddress_country_code : GB\naddress_name : islam dudaeb\nnotify_version : 3.8\ncustom : 12\npayer_status : verified\nbusiness : acjx557-merchant@gmail.com\naddress_country : United Kingdom\nnum_cart_items : 1\nmc_handling1 : 0.00\naddress_city : dsfds\nverify_sign : A--8MSCLabuvN8L.-MHjxC9uypBtAn2ctNFVsJ1VIKS1sfn2UIdYcwD4\npayer_email : acjx557-customer1@gmail.com\nmc_shipping1 : 0.00\ntax1 : 0.00\ntxn_id : 21C8183576407554Y\npayment_type : instant\nlast_name : Customer\naddress_state : sdpioepwo\nitem_name1 : Crew T-shirt\nreceiver_email : acjx557-merchant@gmail.com\npayment_fee : \nquantity1 : 1\nreceiver_id : 57MAV7KHDUDY6\ntxn_type : cart\nmc_gross_1 : 18.00\nmc_currency : GBP\nresidence_country : GB\ntest_ipn : 1\ntransaction_subject : 12\npayment_gross : \nipn_track_id : 237bf1b4ae876', 'VERIFIED', NULL),
(13, 2, '18.00', '2015-11-02 17:57:32', 1, 1, '24V10122PN239350E', 'Completed', 'mc_gross : 18.00\nprotection_eligibility : Eligible\naddress_status : confirmed\nitem_number1 : 5\npayer_id : WBMAT5GBGJ74Y\ntax : 0.00\naddress_street : sdfdsk\r\ndsf;lk\npayment_date : 09:57:52 Nov 02, 2015 PST\npayment_status : Completed\ncharset : windows-1252\naddress_zip : sdf;kl\nmc_shipping : 0.00\nmc_handling : 0.00\nfirst_name : Islam\nmc_fee : 0.81\naddress_country_code : GB\naddress_name : islam dudaeb\nnotify_version : 3.8\ncustom : 13\npayer_status : verified\nbusiness : acjx557-merchant@gmail.com\naddress_country : United Kingdom\nnum_cart_items : 1\nmc_handling1 : 0.00\naddress_city : dsfds\nverify_sign : ALrJLuOnAEp6ftkyfh0BYieCW4xyA2qux2vFQrliuRU1Cp4l.AB23YjE\npayer_email : acjx557-customer1@gmail.com\nmc_shipping1 : 0.00\ntax1 : 0.00\ntxn_id : 24V10122PN239350E\npayment_type : instant\nlast_name : Customer\naddress_state : sdpioepwo\nitem_name1 : Crew T-shirt\nreceiver_email : acjx557-merchant@gmail.com\npayment_fee : \nquantity1 : 1\nreceiver_id : 57MAV7KHDUDY6\ntxn_type : cart\nmc_gross_1 : 18.00\nmc_currency : GBP\nresidence_country : GB\ntest_ipn : 1\ntransaction_subject : 13\npayment_gross : \nipn_track_id : f64af9702678', 'VERIFIED', NULL),
(14, 5, '100.00', '2017-03-22 17:11:18', 1, 0, NULL, NULL, NULL, NULL, NULL),
(15, 5, '100.00', '2017-03-22 17:13:01', 1, 0, NULL, 'Cash', NULL, NULL, NULL),
(16, 6, '18.00', '2017-03-28 20:36:46', 1, 0, NULL, NULL, NULL, NULL, NULL),
(17, 4, '163.00', '2017-04-03 15:36:39', 1, 0, NULL, 'Cash', NULL, NULL, NULL),
(18, 7, '4500.00', '2017-07-23 08:02:23', 1, 0, NULL, NULL, NULL, NULL, NULL),
(19, 7, '4500.00', '2017-07-23 08:02:30', 1, 0, NULL, 'Cash', NULL, NULL, NULL),
(20, 8, '4500.00', '2018-02-03 09:27:01', 1, 0, NULL, 'Cash', NULL, NULL, NULL),
(21, 9, '18290.00', '2018-03-26 06:02:51', 1, 0, NULL, 'Cash', NULL, NULL, NULL),
(22, 9, '1800.00', '2018-03-26 09:27:07', 1, 0, NULL, NULL, NULL, NULL, NULL),
(23, 9, '1800.00', '2018-03-26 09:36:57', 1, 0, NULL, 'Cash', NULL, NULL, NULL),
(24, 9, '6000.00', '2018-06-11 19:00:16', 1, 0, NULL, NULL, NULL, NULL, NULL),
(25, 9, '3600.00', '2018-06-11 19:32:09', 1, 0, NULL, 'Cash', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_products`
--

CREATE TABLE IF NOT EXISTS `orders_products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order` int(10) NOT NULL,
  `product` int(10) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `qty` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `order` (`order`),
  KEY `product` (`product`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `orders_products`
--

INSERT INTO `orders_products` (`id`, `order`, `product`, `price`, `qty`) VALUES
(1, 1, 2, '40.00', 2),
(2, 2, 4, '25.00', 1),
(3, 3, 5, '18.00', 1),
(4, 4, 5, '18.00', 1),
(5, 5, 5, '18.00', 1),
(6, 6, 5, '18.00', 1),
(7, 7, 4, '25.00', 1),
(8, 8, 5, '18.00', 1),
(9, 9, 5, '18.00', 2),
(10, 9, 2, '40.00', 1),
(11, 10, 4, '25.00', 1),
(12, 11, 4, '25.00', 1),
(13, 12, 5, '18.00', 1),
(14, 13, 5, '18.00', 1),
(15, 14, 6, '60.00', 1),
(16, 14, 2, '40.00', 1),
(17, 15, 6, '60.00', 1),
(18, 15, 2, '40.00', 1),
(19, 16, 5, '18.00', 1),
(20, 17, 5, '18.00', 1),
(21, 17, 7, '145.00', 1),
(22, 18, 17, '4500.00', 1),
(23, 19, 17, '4500.00', 1),
(24, 20, 11, '4500.00', 1),
(25, 21, 5, '2500.00', 1),
(26, 21, 4, '2000.00', 1),
(27, 21, 3, '3500.00', 1),
(28, 21, 34, '3600.00', 1),
(29, 21, 41, '2390.00', 1),
(30, 21, 42, '4300.00', 1),
(31, 22, 8, '1800.00', 1),
(32, 23, 8, '1800.00', 1),
(33, 24, 64, '2400.00', 1),
(34, 24, 63, '3600.00', 1),
(35, 25, 74, '3600.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `price` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `category` int(10) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `colour` int(10) DEFAULT NULL,
  `size_number` int(10) DEFAULT NULL,
  `size_letter` int(10) DEFAULT NULL,
  `brand` int(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  KEY `colour` (`colour`),
  KEY `size_number` (`size_number`),
  KEY `size_letter` (`size_letter`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `date`, `category`, `image`, `colour`, `size_number`, `size_letter`, `brand`) VALUES
(74, 'Discovery A6 Promotional', '\r\n\r\n<p>Description</p>\r\n', '3000', '0000-00-00 00:00:00', 43, 'note0.jpg', NULL, NULL, NULL, NULL),
(68, 'Promotional Nifty Note', '<p>Description</p>\r\n', '2600', '0000-00-00 00:00:00', 45, 'note1.jpg', NULL, NULL, NULL, NULL),
(67, 'Swaky Graduation', '<p>Description</p>\r\n', '2600', '0000-00-00 00:00:00', 43, 'note2.jpg', NULL, NULL, NULL, NULL),
(58, 'Swaky Doctor PromotionL', '<p>Description</p>\r\n', '3000', '0000-00-00 00:00:00', 45, 'note3.jpg', NULL, NULL, NULL, NULL),
(57, 'Flux Corporate Notebook', '<p>Description</p>\r\n', '2700', '0000-00-00 00:00:00', 46, 'note4.jpg', NULL, NULL, NULL, NULL),
(77, 'Milestone Corporate', '<p>Description</p>', '3000', '0000-00-00 00:00:00', 46, 'note5.jpg', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `size_letters`
--

CREATE TABLE IF NOT EXISTS `size_letters` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `size_letter` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `size_letters`
--

INSERT INTO `size_letters` (`id`, `size_letter`) VALUES
(1, 'A3'),
(2, 'A4'),
(3, 'A5'),
(4, 'A6');

-- --------------------------------------------------------

--
-- Table structure for table `size_numbers`
--

CREATE TABLE IF NOT EXISTS `size_numbers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `size_number` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `size_numbers`
--

INSERT INTO `size_numbers` (`id`, `size_number`) VALUES
(2, 'A3'),
(3, 'A4'),
(4, 'A5'),
(5, 'A6');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`) VALUES
(1, 'Pending'),
(2, 'Processing'),
(3, 'Dispatched');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `address_1` varchar(255) NOT NULL,
  `address_2` varchar(255) NOT NULL,
  `city` varchar(100) NOT NULL,
  `county` varchar(100) NOT NULL,
  `post_code` varchar(10) NOT NULL,
  `country` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `encode` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `address_1`, `address_2`, `city`, `county`, `post_code`, `country`, `email`, `password`, `date`, `active`, `encode`) VALUES
(4, 'daniel', 'daniel', '2323', '', '33', 'kjfg', '23242424', 'jhj', 'siralidaniel@gmail.com', '0fa76955abfa9dafd83facca8343a92aa09497f98101086611b0bfa95dbc0dcc661d62e9568a5a032ba81960f3e55d4a', '2017-03-22 17:04:55', 1, '21528504020170322170455926806155'),
(5, 'knknk', 'n', 'knkn', 'nknk', 'kn', 'knk', 'knkn', 'lknln', 'ddd@gmail.com', '504f008c8fcf8b2ed5dfcde752fc5464ab8ba064215d9c5b5fc486af3d9ab8c81b14785180d2ad7cee1ab792ad44798c', '2017-03-22 17:10:34', 1, '630585692201703221710341866918917'),
(9, 'huncho', 'quavo', '999', '000', 'nakuru', 'njoro', 'yuio', 'Kenya', 'ian@gmail.com', '27f9e9353772780c0e803cee347fb6b22fea420355318b03857ee5ee2a45a7aac9062d466bf343c9fa5bc60d2b359b51', '2018-02-09 18:56:03', 1, '1518248008201802091856032118057794'),
(8, 'sss', 's', '23', '184', 'Nairobi', 'knk', '134', 'ken', 'venlo@gmail.com', '0fa76955abfa9dafd83facca8343a92aa09497f98101086611b0bfa95dbc0dcc661d62e9568a5a032ba81960f3e55d4a', '2017-08-01 07:04:40', 1, '1985618251201708010704401529510760');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
