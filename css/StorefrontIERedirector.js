var StorefrontIERedirector = (function () {
    function StorefrontIERedirector(jqueryBrowserplugin, redirectMappings) {
        this.redirectMappings = "";
        this.browser = jqueryBrowserplugin;
        this.redirectMappings = redirectMappings;
    }
    StorefrontIERedirector.prototype.RedirectToRelevantPage = function () {
        if (this.DetectIE8() && this.redirectMappings != undefined && this.redirectMappings != "") {
            var storefrontUrl = this.UrlRelativePath();
            if (storefrontUrl != '') {
                var urlMappings = this.redirectMappings.split(",");
                for (var i = 0, len = urlMappings.length; i < len; i++) {
                    var storePageUrlIndex = 0;
                    var storePageRedirectUrlIndex = 1;
                    var urlMap = urlMappings[i].split("|");
                    if (urlMap[storePageUrlIndex].toUpperCase().replace(" ", "") == storefrontUrl.toUpperCase().replace(" ", "")) {
                        window.location.href = urlMap[storePageRedirectUrlIndex];
                    }
                }
            } //----
        } //---
    }; //----------------------------------------
    StorefrontIERedirector.prototype.DetectIE8 = function () {
        return (this.browser.msie && this.browser.version == "8.0");
    };
    StorefrontIERedirector.prototype.UrlRelativePath = function () {
        return window.location.pathname + window.location.search;
    };
    return StorefrontIERedirector;
})();
//# sourceMappingURL=StorefrontIERedirector.js.map