﻿(function ($) {

    //commented out sections are an attempt to refactor the code so it repeated itself less - but this caused issues with form submission and IE & Firefox functionality
    var methods = {
        init: function (options) {

            //var types = ["Text", "Radio", "Checkbox", "Date", "Datetime-local", "Email", "Month", "Number", "Range", "Tel", "Time", "Url", "Week"];
            //var field = this;
            //var input = this[0].type;

            // types.forEach(function(type) {
            //    if (type.toLowerCase() == input)
            //    {
            //        var className = "required" + type;
            //        var datetimeClass = "requiredDatetime-local";
            //        if (field.hasClass(className) || field.hasClass(datetimeClass)) {
            //            field.blur(function () { methods.required(type.toLowerCase(), field); });
            //            field.keyup(function () { methods.required(type.toLowerCase(), field); });
            //            field.change(function () { methods.required(type.toLowerCase(), field); });
            //        }
            //    }
            // });

            switch (this[0].type) {
                //input of type text                                                                                    
                case "text":
                    //Set events to fire validation
                    //IE and Firefox do not support most types of HTML5 input and they default to textboxes, so we must handle these using class names instead
                    if (this.hasClass("requiredDate")) {
                        this.blur(function () { methods.requiredDate(this); });
                        this.keyup(function () { methods.requiredDate(this); });
                        this.change(function () { methods.requiredDate(this); });
                    }
                    if (this.hasClass("requiredDatetime-local")) {
                        this.blur(function () { methods.requiredDateTime(this); });
                        this.keyup(function () { methods.requiredDateTime(this); });
                        this.change(function () { methods.requiredDateTime(this); });
                    }
                    if (this.hasClass("requiredMonth")) {
                        this.blur(function () { methods.requiredMonth(this); });
                        this.keyup(function () { methods.requiredMonth(this); });
                        this.change(function () { methods.requiredMonth(this); });
                    }
                    if (this.hasClass("requiredTime")) {
                        this.blur(function () { methods.requiredTime(this); });
                        this.keyup(function () { methods.requiredTime(this); });
                        this.change(function () { methods.requiredTime(this); });
                    }
                    if (this.hasClass("requiredWeek")) {
                        this.blur(function () { methods.requiredWeek(this); });
                        this.keyup(function () { methods.requiredWeek(this); });
                        this.change(function () { methods.requiredWeek(this); });
                    }
                    if (this.hasClass("requiredNumber") || this.hasClass("requiredRange")) {
                        this.blur(function () { methods.requiredNumber(this); });
                        this.keyup(function () { methods.requiredNumber(this); });
                    }
                    if (this.hasClass("requiredTel")) {
                        this.blur(function () { methods.requiredTel(this); });
                        this.keyup(function () { methods.requiredTel(this); });
                    }
                    if (this.hasClass("requiredUrl")) {
                        this.blur(function () { methods.requiredLink(this); });
                        this.keyup(function () { methods.requiredLink(this); });
                    }
                    else if (this.hasClass("required")) {
                        this.blur(function () { methods.requiredTextBox(this); });
                        this.keyup(function () { methods.requiredTextBox(this); });

                        if (this.hasClass("requiredEmail")) {
                            this.blur(function () { methods.requiredEmail(this); });
                            this.keyup(function () { methods.requiredEmail(this); });
                        }
                    }

                    break;
                case "email":
                    this.blur(function () { methods.requiredEmail(this); });
                    this.keyup(function () { methods.requiredEmail(this); });
                    break;
                case "url":
                    this.blur(function () { methods.requiredLink(this); });
                    this.keyup(function () { methods.requiredLink(this); });
                    break;
                case "tel":
                    this.blur(function () { methods.requiredTel(this); });
                    this.keyup(function () { methods.requiredTel(this); });
                    break;
                case "date":
                    this.blur(function () { methods.requiredDate(this); });
                    this.keyup(function () { methods.requiredDate(this); });
                    this.change(function () { methods.requiredDate(this); });
                    break;
                case "datetime-local":
                    this.blur(function () { methods.requiredDateTime(this); });
                    this.keyup(function () { methods.requiredDateTime(this); });
                    this.change(function () { methods.requiredDateTime(this); });
                    break;
                case "month":
                    this.blur(function () { methods.requiredMonth(this); });
                    this.keyup(function () { methods.requiredMonth(this); });
                    this.change(function () { methods.requiredMonth(this); });
                    break;
                case "time":
                    this.blur(function () { methods.requiredTime(this); });
                    this.keyup(function () { methods.requiredTime(this); });
                    this.change(function () { methods.requiredTime(this); });
                    break;
                case "week":
                    this.blur(function () { methods.requiredWeek(this); });
                    this.keyup(function () { methods.requiredWeek(this); });
                    this.change(function () { methods.requiredWeek(this); });
                    break;
                case "number":
                    this.blur(function () { methods.requiredNumber(this); });
                    this.keyup(function () { methods.requiredNumber(this); });
                    break;
                case "range":
                    this.blur(function () { methods.requiredRange(this); });
                    this.keyup(function () { methods.requiredRange(this); });
                    this.change(function () { methods.requiredRange(this); });
                    break;
                //case "radio":
                //    this.change(function () { methods.requiredRadio(this); });
                //    break;
                //case "checkbox":
                //    this.change(function () { methods.requiredCheck(this); });
                //    break;
                //case "select-one":
                //    //this.click(function () { alert("ff"); });
                //    break;

                //case "textarea":
                //    // this.click(function () { alert("ss"); });
                //    break;

            }

            
            if (this[0].type != "radio" && this[0].type != "checkbox" && this[0].type != "range" && this[0].type != "submit" && this[0].type != "color") {
                var resultIcon = $("<div />");
                $(resultIcon).addClass("resultIcon");
                $(this).after(resultIcon);
                //Text box text/content can't run under the icon - add padding and then resize to original size so we don't interfere with the design
                this.css("padding-right", $(resultIcon).width() + parseInt(this.css("padding-right").replace("px", "")));
                this.width(this.width() - $(resultIcon).width());

                ////$(resultIcon).css("margin-top", $(this).height());
                //$(resultIcon).css("margin-left", -$(resultIcon).width() - 11);
                

                //If tooltip is required, bind this to the result icon
                if ($(this[0]).attr("data-error").length > 0) {
                    $(resultIcon).attr("data-description", $(this[0]).attr("data-error"));
                    var popupAlign = "right";
                    if ((typeof options !== 'undefined') && (typeof options.popAlign !== 'undefined'))
                        popupAlign = options.popAlign;
                    $(resultIcon).NextHelperPopUp({ 'width': 150, 'align': popupAlign });
                }
            }
            else {
                if ($(this[0]).attr("data-error").length > 0) {
                    var errorMessage = $("<div />");
                    $(errorMessage).addClass("validationError");
                    $(this).after(errorMessage);
                    $(errorMessage).css("clear", 'both');
                    $(errorMessage).text($(this[0]).attr("data-error"));
                    $('.validationError').hide();
                }
            }

            return this;

        },

        showValidationResult: function (target, pass) {

            if (pass) {
                $(target).next(".resultIcon").addClass("valid");
                $(target).next(".resultIcon").removeClass("error");
                $(target).css("border", "");
                $(target).next(".validationError").hide();
            } else {

                $(target).next(".resultIcon").removeClass("valid");
                $(target).next(".resultIcon").addClass("error");
                $(target).css("border", "2px #be0000 solid");
                $(target).next('.validationError').show();
            }

        },
        displayErrorDialogue: function (control) {

            var settings = arguments.length > 1 
                ? arguments[1]
                : {
                    'target': '',
                    'align': 'right',
                    'autoClose': false,
                    'width': 120,
                    'errorMessage': true
                };

            showDialoguePopUp(control, settings);

            setTimeout(function () {
                hideDialoguePopUp(control);
            }, 2000);

            $(control).change(function () {
                methods.requiredDropDown(this);
            });
        },
        hideErrorDialogue: function (control) {
            hideDialoguePopUp(control);
        },
        //required: function (field, value) {

        //    var regExps = [
        //        { "text": new RegExp(/[a-zA-Z]{1,250}/) },
        //        { "number": new RegExp(/[0-9]+/) },
        //        { "url": new RegExp(/http:\/\/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/) },
        //        { "email": new RegExp(/^[A-Za-z0-9\-\_\.]+@([A-Za-z0-9\-\_\.]+\.)+[A-Za-z\.]+$/) },
        //        { "tel": new RegExp(/^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$/) },
        //        { "date": new RegExp(/(\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b)|(\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b$)/) },
        //        { "datetime-local": new RegExp(/(\d{1,2}[\/-]\d{1,2}[\/-]\d{4} ([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])|(\d{4}[\/-]\d{1,2}[\/-]\d{1,2}[T]([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])/) },
        //        { "time": new RegExp(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/) },
        //        { "week": new RegExp(/(Week \d{1,2}, \d{4})|(\d{4}-W(([0-4][0-9])|([5][0-3])))/) },
        //        { "month": new RegExp(/(\b\d{1,2}\D{0,3})?\b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)\D?(\d{1,2}\D?)?\D?((19[7-9]\d|20\d{2})|\d{2})|(\b\d{4}[\/-]\d{1,2}\b)/) }
        //    ];
            
     
        //    if (hasValChanged(value))
        //    {
        //        if ($(value).val().length == 0) {
        //            methods.showValidationResult(value, false);
        //            return false;
        //        }

        //        if (field == "checkbox") {
        //            if (value[0].checked) {
        //                methods.showValidationResult(value, true);
        //                return true;
        //            }
        //            else
        //            {
        //                methods.showValidationResult(value, false);
        //                return false;
        //            }
        //        }

        //        if (field == "radio") {
        //            if($("input[type=radio]:checked").length > 0)
        //            {
        //                methods.showValidationResult(value, true);
        //                return true;
        //            }
        //            else
        //            {
        //                methods.showValidationResult(value, false);
        //                return false;
        //            }
        //        }

        //                    $.each(regExps, function () {
        //                        $.each(this, function (type, regExp) {
        //                            if (type == field) {
        //                                if (value[0].value.match(regExp)) {
        //                                    if (field == "date") {
        //                                        var isValid = null;
        //                                        if (isValid && $(value).data("validation-maxdate")) {
        //                                            isValid = this.requiredDateRange(value, true);
        //                                        }

        //                                        if (isValid && $(value).data("validation-mindate")) {
        //                                            isValid = this.requiredDateRange(value, false);
        //                                        }
        //                                        else {
        //                                            methods.showValidationResult(value, true);
        //                                            isValid = true;
        //                                        }

        //                                        return isValid;
        //                                    }

        //                                    methods.showValidationResult(value, true);
        //                                    return true;
                                            
        //                                }
        //                            }
        //                        });
        //                    });

        //            }
        //    else
        //    {
        //        methods.showValidationResult(value, false);
        //        return false;
        //    }
        //},

        requiredTextBox: function (textBox) {

            //Make sure default value has changed
            //if ($(textBox).val().length > 0 && $(textBox).attr("data-default").length > 0) {
            //    if ($(textBox).val() == $(textBox).attr("data-default")) {
            //        methods.showValidationResult(textBox, false);
            //        return false;
            //    }
            //}

            if (hasValChanged(textBox)) {
                //Basic required textbox validator
                if ($(textBox).val().length == 0) {
                    methods.showValidationResult(textBox, false);

                    //                if ($(textBox).attr("data-error").length > 0) {
                    //                    $(textBox).next(".resultIcon").attr("data-description", $(textBox).attr("data-error"));
                    //                }

                    return false;
                } else {
                    methods.showValidationResult(textBox, true);
                    //                if ($(textBox).attr("data-valid").length > 0) {
                    //                    $(textBox).next(".resultIcon").attr("data-description", $(textBox).attr("data-valid"));
                    //                }
                    return true;
                };
            }
            else {
                methods.showValidationResult(textBox, false);
                return false;
            }

        },

        requiredEmail: function (textBox) {
            var tbVal = $(textBox).val();
            var emailregex = new RegExp(/^[A-Za-z0-9\-\_\.]+@([A-Za-z0-9\-\_\.]+\.)+[A-Za-z\.]+$/);

            if (hasValChanged(textBox)) {
                if (tbVal.match(emailregex)) {
                    methods.showValidationResult(textBox, true);
                    return true;
                } else {
                    methods.showValidationResult(textBox, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(textBox, false);
                return false;
            }

        },

        requiredLink: function (textBox) {
            var tbVal = $(textBox).val();
            var linkregex = new RegExp(/http:\/\/[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/);

            if (hasValChanged(textBox)) {
                if (tbVal.match(linkregex)) {
                    methods.showValidationResult(textBox, true);
                    return true;
                } else {
                    methods.showValidationResult(textBox, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(textBox, false);
                return false;
            }
        
        },

        requiredTel: function (textBox) {
            var tbVal = $(textBox).val();
            var telregex = new RegExp(/^(?:(?:\(?(?:0(?:0|11)\)?[\s-]?\(?|\+)44\)?[\s-]?(?:\(?0\)?[\s-]?)?)|(?:\(?0))(?:(?:\d{5}\)?[\s-]?\d{4,5})|(?:\d{4}\)?[\s-]?(?:\d{5}|\d{3}[\s-]?\d{3}))|(?:\d{3}\)?[\s-]?\d{3}[\s-]?\d{3,4})|(?:\d{2}\)?[\s-]?\d{4}[\s-]?\d{4}))(?:[\s-]?(?:x|ext\.?|\#)\d{3,4})?$/);

            if (hasValChanged(textBox)) {
                if (tbVal.match(telregex)) {
                    methods.showValidationResult(textBox, true);
                    return true;
                } else {
                    methods.showValidationResult(textBox, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(textBox, false);
                return false;
            }

        },

        requiredDropDown: function (dropDown, errorDialogSettings) {
            var ddlVal = $(dropDown).val();
            if (ddlVal == "" || ddlVal == null) {

                methods.displayErrorDialogue(dropDown, errorDialogSettings);

                //Check for cutsom drop down
                var customDDL = $(dropDown).parent("div.dk_container");
                if (customDDL.length > 0) {
                    var customLink = customDDL.find("a.dk_toggle");
                    $(customLink).addClass("error");
                } else {
                    $(dropDown).addClass("error");
                }
                return false;
            } else {
                var customDDL = $(dropDown).parent("div.dk_container");
                if (customDDL.length > 0) {
                    var customLink = customDDL.find("a.dk_toggle");
                    $(customLink).removeClass("error");
                }
                else {
                    $(dropDown).removeClass("error");
                }
                methods.hideErrorDialogue(dropDown);
                return true;
            }
        },

        requiredDate: function (datePicker) {
            var dpVal = $(datePicker).val();

            //var origDateRegEx = new RegExp(/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/);
            var dateRegEx = new RegExp(/(\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b)|(\b\d{4}[\/-]\d{1,2}[\/-]\d{1,2}\b$)/);

            if (hasValChanged(datePicker)) {
                if (dpVal.match(dateRegEx)) {

                    var isValid = true;

                    if (isValid && $(datePicker).data("validation-maxdate")) {
                        isValid = this.requiredDateRange(datePicker, true);
                    }

                    if (isValid && $(datePicker).data("validation-mindate")) {
                        isValid = this.requiredDateRange(datePicker, false);
                    }

                    if (isValid) {
                        methods.showValidationResult(datePicker, true);
                    }

                    return isValid;
                }
                else {
                    methods.showValidationResult(datePicker, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(datePicker, false);
                return false;
            }
        },

        //"requiredDateRange" Function validates whether a date value is greater/less than a limit rule supplied in a data-* (validation-maxdate / validation-mindate)
        //Current rules:
        //  "today" - The current date will be the min/max limit
        //  "(+/-)(int)d - The limit will be plus/minus x number of days compared to today. E.g "+60d" will be 60 days into the future.
        //-------
        //isMax = If true, validation is for the maximum limit. If false, validation is for the minimum limit.
        requiredDateRange: function (textbox, isMax) {
            var $textbox = $(textbox);

            var dateLimitRule;
            if (isMax) {
                dateLimitRule = $textbox.data("validation-maxdate");
            }
            else {
                dateLimitRule = $textbox.data("validation-mindate");
            }

            if (dateLimitRule) {

                var dmy = $textbox.val().split("/");
                var selectedDate = new Date(parseInt(dmy[2], 10), parseInt(dmy[1], 10) - 1, parseInt(dmy[0], 10));

                //Check date is valid.
                if (selectedDate != "Invalid Date") {

                    var dt = new Date();

                    var dateLimit = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());

                    //Regex rule
                    var regexRule = new RegExp(/^(\+|\-)([0-9]*)(d)$/) //(xx)d e.g +60d or -45d

                    if (dateLimitRule == "today") {
                        dateLimit = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate());
                    }
                    else if (regexRule.test(dateLimitRule)) {

                        var plusMinus = dateLimitRule.match(regexRule)[1]

                        switch (dateLimitRule.match(regexRule)[3]) {
                            case "d": //days
                                var days = dateLimitRule.match(regexRule)[2]; //returns integer part of regex rule

                                switch (plusMinus) {
                                    case "+":
                                        dateLimit.setDate((new Date()).getDate() + (parseInt(days)));
                                        break;
                                    case "-":
                                        dateLimit.setDate((new Date()).getDate() - (parseInt(days)));
                                        break;
                                }


                                break;
                        }

                    }

                    if (isMax) {
                        //check whether dateLimit is less than selectedDate
                        //If so, return false, display validation error
                        if (dateLimit.getTime() < selectedDate.getTime()) {
                            methods.showValidationResult(textbox, false);
                            return false;
                        }

                    }
                    else {
                        //check whether dateLimit is greater than selectedDate
                        //If so, return false, display validation error
                        if (dateLimit.getTime() > selectedDate.getTime()) {
                            methods.showValidationResult(textbox, false);
                            return false;
                        }
                    }


                }

            }

            return true;

        },

        requiredDateTime: function (datetimePicker) {
            var datetimeVal = $(datetimePicker).val();
            var datetimeRegex = new RegExp(/(\d{1,2}[\/-]\d{1,2}[\/-]\d{4} ([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])|(\d{4}[\/-]\d{1,2}[\/-]\d{1,2}[T]([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9])/);

            if (hasValChanged(datetimePicker)) {
                if (datetimeVal.match(datetimeRegex)) {
                    methods.showValidationResult(datetimePicker, true);
                    return true;
                }
                else {
                    methods.showValidationResult(datetimePicker, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(datetimePicker, false);
                return false;
            }
        },

        requiredNumber: function (number) {
            var numberVal = $(number).val();
            
            if (hasValChanged(number)) {
                if (numberVal != "") {
                    methods.showValidationResult(number, true);
                    return true;
                }
                else {
                    methods.showValidationResult(number, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(number, false);
                return false;
            }
        },

        requiredRange: function (slider) {
            var numberVal = $(slider).val();

            if (numberVal != "") {
                methods.showValidationResult(slider, true);
                return true;
            }
            else {
                methods.showValidationResult(slider, false);
                return false;
            }
        },

        requiredTime: function (time) {
            var timeVal = $(time).val();
            var timeRegex = new RegExp(/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/);

            if (hasValChanged(time)) {
                if (timeVal.match(timeRegex)) {
                    methods.showValidationResult(time, true);
                    return true;
                }
                else {
                    methods.showValidationResult(time, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(time, false);
                return false;
            }
           
        },

        requiredWeek: function (week) {
            var weekVal = $(week).val();
            var weekRegex = new RegExp(/(Week \d{1,2}, \d{4})|(\d{4}-W(([0-4][0-9])|([5][0-3])))/);

            if (hasValChanged(week)) {
                if (weekVal.match(weekRegex)) {
                    methods.showValidationResult(week, true);
                    return true;
                }
                else {
                    methods.showValidationResult(week, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(week, false);
                return false;
            }
        },

        requiredMonth: function (month) {
            var monthVal = $(month).val();
            var monthRegex = new RegExp(/(\b\d{1,2}\D{0,3})?\b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(Nov|Dec)(?:ember)?)\D?(\d{1,2}\D?)?\D?((19[7-9]\d|20\d{2})|\d{2})|(\b\d{4}[\/-]\d{1,2}\b)/);

            if (hasValChanged(month)) {
                if (monthVal.match(monthRegex)) {
                    methods.showValidationResult(month, true);
                    return true;
                }
                else {
                    methods.showValidationResult(month, false);
                    return false;
                }
            }
            else {
                methods.showValidationResult(month, false);
                return false;
            }
        },

        requiredCheck: function (check) {
            var checked = check[0].checked;

            if (checked)
            {
                methods.showValidationResult(check, true);
                return true;
            }
            else {
                methods.showValidationResult(check, false);
                return false;
            }
        },

        requiredRadio: function (radio) {
            if($("input[type=radio]:checked").length > 0)
            {
                methods.showValidationResult(radio, true);
                return true;
            }
            else {
                methods.showValidationResult(radio, false);
                return false;
            }
        },
      
        //This allows us to call the 'validate' method from outside the plugin e.g. from a form submit
        validate: function () {

            //var types = ["Text", "Radio", "Checkbox", "Date", "Datetime-local", "Email", "Month", "Number", "Range", "Tel", "Time", "Url", "Week"];
            //var field = this;
            //var input = this[0].type;

            //types.forEach(function (type) {
            //    if (type.toLowerCase() == input) {
            //        var className = "required" + type;
            //        var datetimeClass = "requiredDatetime-local";
            //        if (field.hasClass(className) || field.hasClass(datetimeClass)) {
            //            return methods.required(type.toLowerCase(), field);
            //        }
            //    }
            //});

            //if (input == "select-one") {
            //    return methods.requiredDropDown(this, arguments.length > 0 ? arguments[0] : false);
            //}

            switch (this[0].type) {
                case "text":
                    if (this.hasClass("requiredEmail")) {
                        return methods.requiredEmail(this);
                    }

                    if (this.hasClass("requiredDate")) {
                        return methods.requiredDate(this);
                    }

                    if (this.hasClass("requiredDatetime-local")) {
                        return methods.requiredDateTime(this);
                    }

                    if (this.hasClass("requiredMonth")) {
                        return methods.requiredMonth(this);
                    }

                    if (this.hasClass("requiredWeek")) {
                        return methods.requiredWeek(this);
                    }

                    if (this.hasClass("requiredTel")) {
                        return methods.requiredTel(this);
                    }

                    if (this.hasClass("requiredNumber") || this.hasClass("requiredRange")) {
                        return methods.requiredNumber(this);
                    }

                    if (this.hasClass("requiredTime")) {
                        return methods.requiredTime(this);
                    }

                    if (this.hasClass("requiredUrl")) {
                        return methods.requiredLink(this);
                    }

                    return methods.requiredTextBox(this);
                    break;
                case "select-one":
                    return methods.requiredDropDown(this, arguments.length > 0 ? arguments[0] : false);
                    break;
                case "email":
                    return methods.requiredEmail(this);
                    break;
                case "url":
                    return methods.requiredLink(this);
                    break;
                case "tel":
                    return methods.requiredTel(this);
                    break;
                case "date":
                    return methods.requiredDate(this);
                    break;
                case "datetime-local":
                    return methods.requiredDateTime(this);
                    break;
                case "number":
                    return methods.requiredNumber(this);
                    break;
                case "range":
                    return methods.requiredRange(this);
                    break;
                case "time":
                    return methods.requiredTime(this);
                    break;
                case "week":
                    return methods.requiredWeek(this);
                    break;
                case "month":
                    return methods.requiredMonth(this);
                    break;
                case "radio":
                    return methods.requiredRadio(this);
                    break;
                case "checkbox":
                    return methods.requiredCheck(this);
                    break;
                case "":
                    break;
            }

            //return true;
        }
    };

    $.fn.validateControl = function (method) {

        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.validateControl');
        }
    };

    hasValChanged = function (textbox) {
        if ($(textbox).val().length > 0 && $(textbox).attr("data-default") && $(textbox).attr("data-default").length > 0) {
            if ($(textbox).val() == $(textbox).attr("data-default")) {
                return false;
            }
            else
                return true;
        }
        return true;
    }

})(jQuery);


$(document).ready(function () {
//    $(".required").each(function () {
//        $(this).validateControl();
//    });

    //$(".required").validateControl();        
});



