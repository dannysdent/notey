var ABCPlatformConfigSettings = {
primary: "www.next.co.uk",
secondary: "www2.next.co.uk",
tertiary: "www3.next.co.uk",
versionNumber: "96",
DrainPeriod: 3600,
DrainCutoffPeriod: 7200,
divertPercentage: 20,
ABCTesting: true,
DevicePreference: "Both" 
}
