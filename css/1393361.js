function getCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }else{
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
          end = dc.length;
        }
    }
    // because unescape has been deprecated, replaced with decodeURI
    //return unescape(dc.substring(begin + prefix.length, end));
    return decodeURI(dc.substring(begin + prefix.length, end));
} 

function createCookie(name,value,days) {
  if (days > 1) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "expires="+date.toGMTString();
  } else if (days == -1) {
    var expires = "expires=Thu, 1 Jan 1970 12:00:00 UTC";
  }
  else var expires = "";
  document.cookie = name+"="+value+ ";" + expires+";"+"path=/;";
}

$( document ).ready(function() {
    var cookieName = "mt_search_cookie";
    var cookieValue = "SLI";
    var testCookie = getCookie(cookieName);
    createCookie(cookieName, cookieValue, 30);
    /*TrackGAEvent("MT-68856", "cookie", "Dropped Cookie - SLI", 0);*/
});